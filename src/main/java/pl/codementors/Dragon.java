package pl.codementors;

/**
 * Class describing Dragon.
 *
 * @author Maciek
 */
public class Dragon {
    /**
     * Dragon's name.
     */
    private String name;

    /**
     * Dragon's color.
     */
    private String color;

    /**
     * Dragon's age.
     */
    private int age;

    /**
     * Dragon's wingspan.
     */
    private int wingspan;

    /**
     * Empty constructor.
     */
    public Dragon() {
    }

    /**
     *
     * @param name Dragon's name.
     * @param color Dragon's color.
     * @param age Dragon's age.
     * @param wingspan Dragon's wingspan.
     */
    public Dragon(String name, String color, int age, int wingspan) {
        this.name = name;
        this.color = color;
        this.age = age;
        this.wingspan = wingspan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }
}
