package pl.codementors;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class describing catalog of dragons.
 *
 * @author Maciek
 */
public class DragonsCatalog {

    /**
     * List of dragons.
     */
    private List<Dragon> dragonsList = new ArrayList<>();

    public List<Dragon> getDragonsList() {
        return dragonsList;
    }

    /**
     * Add dragon to catalog.
     *
     * @param dragon Dragon to be added.
     */
    public void addDragon(Dragon dragon) {
        dragonsList.add(dragon);
    }

    /**
     * Remove dragon from catalog.
     *
     * @param dragon Dragon to be removed.
     */
    public void removeDragon(Dragon dragon) {
        dragonsList.remove(dragon);
    }

    /**
     * @return Returns oldest dragon.
     */
    public Dragon getOldest() {
        return dragonsList.stream().filter(value -> value.getAge() ==
                (dragonsList.stream().mapToInt(value2 -> value2.getAge()).max().orElse(-1))).findFirst().orElse(null);
    }

    /**
     * @return Returns max wingspan.
     */
    public int getMaxWingspan() {
        return dragonsList.stream().mapToInt(value -> value.getWingspan()).max().orElse(-1);
    }

    /**
     * @return Returns number of letters in longest name.
     */
    public int getLongestNameNumberOfLetters() {
        return dragonsList.stream().mapToInt(value -> value.getName().length()).max().orElse(-1);
    }

    /**
     * @return Returns list of dragons sorted by age.
     */
    public List<Dragon> sortByAge() {
        List<Dragon> sortedDragons;
        sortedDragons = dragonsList.stream().sorted((i1, i2) -> i1.getAge() - i2.getAge()).collect(Collectors.toList());
        return sortedDragons;
    }

    /**
     * @param color Color of searched dragons.
     * @return Returns list of dragons specified by color.
     */
    public List<Dragon> getDragonsByColor(String color) {
        List<Dragon> dragonsByColor;
        dragonsByColor = dragonsList.stream().filter(value -> value.getColor().equals(color)).collect(Collectors.toList());
        return dragonsByColor;
    }

    /**
     * @return Returns list of dragons names.
     */
    public List<String> getDragonsNames() {
        List<String> dragonsNames;
        dragonsNames = dragonsList.stream().map(value -> value.getName()).collect(Collectors.toList());
        return dragonsNames;
    }

    /**
     * @return Returns list of dragons color in upper case.
     */
    public List<String> getDragonsColorInUpperCase() {
        List<String> dragonsColor;
        dragonsColor = dragonsList.stream().map(value -> value.getColor().toUpperCase()).collect(Collectors.toList());
        return dragonsColor;
    }

    /**
     *
     * @param age Age to be checked.
     * @return Returns true if all dragons are older than age specified as a parameter.
     */
    public boolean checkIfAllOlderThan(int age) {
        return dragonsList.stream().allMatch(value -> value.getAge() > age);
    }

    /**
     *
     * @param color Color to be checked.
     * @return Returns true if any dragon is a color that has been specified as a parameter.
     */
    public boolean checkIfAnyIsColorOf(String color) {
        return dragonsList.stream().anyMatch(value -> value.getColor().equals(color));
    }
}
