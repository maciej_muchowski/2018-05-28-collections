package pl.codementors;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import java.util.List;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * @author Maciek
 */
@RunWith(PowerMockRunner.class)
public class DragonCatalogTest {
    DragonsCatalog dragonsCatalog;
    Dragon dragon1, dragon2, dragon3, dragon4;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @Before
    public void prepare() {
        dragonsCatalog = new DragonsCatalog();
        dragon1 = new Dragon("Paarthurnax", "Gray", 150, 30);
        dragon2 = new Dragon("Durnehviir", "Gray", 120, 25);
        dragon3 = new Dragon("Alduin", "Black", 130, 27);
        dragon4 = new Dragon("Vulthuryol", "Red", 100, 24);
    }

    @Test
    public void dragonCatalogWhenCreated() {
        assertThat(dragonsCatalog.getDragonsList().size(), is(0));
        assertThat(dragonsCatalog.getDragonsList().isEmpty(), is(true));
    }

    @Test
    public void addDragon() {
        dragonsCatalog.addDragon(dragon1);
        assertThat(dragonsCatalog.getDragonsList().size(), is(1));
        assertThat(dragonsCatalog.getDragonsList(), hasItem(dragon1));
    }

    @Test
    public void removeDragon() {
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.removeDragon(dragon1);
        assertThat(dragonsCatalog.getDragonsList().size(), is(0));
        assertThat(dragonsCatalog.getDragonsList().isEmpty(), is(true));
    }

    @Test
    public void getOldest() {
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        assertThat(dragonsCatalog.getOldest(), is(dragon1));
    }

    @Test
    public void getOldestWhenNoDragonInCatalog() {
        assertThat(dragonsCatalog.getOldest(), is(nullValue()));
    }

    @Test
    public void getMaxWingspan() {
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        assertThat(dragonsCatalog.getMaxWingspan(), is(dragon1.getWingspan()));
    }

    @Test
    public void getMaxWingspanWhenNoDragonInCatalog() {
        assertThat(dragonsCatalog.getMaxWingspan(), is(-1));
    }

    @Test
    public void getLongestNameNumberOfLetters() {
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        assertThat(dragonsCatalog.getLongestNameNumberOfLetters(), is(dragon1.getName().length()));
    }

    @Test
    public void getLongestNameNumberOfLettersWhenNoDragonsInCatalog() {
        assertThat(dragonsCatalog.getLongestNameNumberOfLetters(), is(-1));
    }

    @Test
    public void sortByAge() {
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        dragonsCatalog.addDragon(dragon3);
        dragonsCatalog.addDragon(dragon4);
        List<Dragon> sortedDragons = dragonsCatalog.sortByAge();
        assertNotEquals(sortedDragons, dragonsCatalog.getDragonsList());
        assertThat(sortedDragons, hasItems(dragon1, dragon2, dragon3, dragon4));
    }

    @Test
    public void getDragonsByColor() {
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        dragonsCatalog.addDragon(dragon3);
        List<Dragon> dragonsByColor = dragonsCatalog.getDragonsByColor("Gray");
        assertThat(dragonsByColor, hasItems(dragon1, dragon2));
        assertThat(dragonsByColor.size(), is(2));
        assertFalse(dragonsByColor.contains(dragon3));
    }

    @Test
    public void getDragonsNames() {
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        List<String> dragonNames = dragonsCatalog.getDragonsNames();
        assertThat(dragonNames, hasItems(dragon1.getName(), dragon2.getName()));
        assertFalse(dragonNames.contains(dragon1.getColor()));
    }

    @Test
    public void getDragonsColorInUpperCase() {
        dragonsCatalog.addDragon(dragon1);
        List<String> dragonNames = dragonsCatalog.getDragonsColorInUpperCase();
        assertThat(dragonNames, hasItem(dragon1.getColor().toUpperCase()));
        assertFalse(dragonNames.contains(dragon1.getColor()));
    }

    @Test
    public void checkIfAllOlderThanWhenResultShouldBeTrue(){
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        dragonsCatalog.addDragon(dragon3);
        dragonsCatalog.addDragon(dragon4);
        assertTrue(dragonsCatalog.checkIfAllOlderThan(50));
    }

    @Test
    public void checkIfAllOlderThanWhenResultShouldBeFalse(){
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        dragonsCatalog.addDragon(dragon3);
        dragonsCatalog.addDragon(dragon4);
        assertFalse(dragonsCatalog.checkIfAllOlderThan(200));
    }

    @Test
    public void checkIfAnyIsColorOfWhenResultShouldBeTrue(){
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        dragonsCatalog.addDragon(dragon3);
        dragonsCatalog.addDragon(dragon4);
        assertTrue(dragonsCatalog.checkIfAnyIsColorOf("Black"));
    }

    @Test
    public void checkIfAnyIsColorOfWhenResultShouldBeFalse(){
        dragonsCatalog.addDragon(dragon1);
        dragonsCatalog.addDragon(dragon2);
        dragonsCatalog.addDragon(dragon3);
        dragonsCatalog.addDragon(dragon4);
        assertFalse(dragonsCatalog.checkIfAnyIsColorOf("Blue"));
    }
}
